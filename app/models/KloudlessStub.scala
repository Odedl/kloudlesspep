package models

import javax.inject.Inject

import play.api.Configuration
import play.api.libs.json.{JsValue, Json, JsObject}
import play.api.libs.ws.WSAPI
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
/**
 * Created by oded on 8/25/16.
 */
class KloudlessStub @Inject()(configuration: Configuration, ws: WSAPI) {

    val kloudless_base_url = configuration.getString("kloudless_base_url").get
    val page_size = 1

    def getRecentFiles(get_recent_files_request: KloudlessRecentFilesRequest, start_time_date_formatted: String): Future[JsValue] = {
        val get_recent_files_url = s"${kloudless_base_url}${get_recent_files_request.account_id}/recent?after=${start_time_date_formatted}"
        ws.url(get_recent_files_url).withMethod("GET").withHeaders(("Authorization", "Bearer JT5ZnrN8bsa2lNSBnctU1cawUjzrpD")).withQueryString(("page_size",
            page_size.toString)).execute
            .map(response => {
            response.status match {
                case 200 =>
                    Json.parse(response.body)
                case _=>
                    throw new Exception
            }
        }
        )
    }
}
