package models

import java.util.Date

/**
 * Created by oded on 8/24/16.
 */
case class KloudlessRecentFilesRequest(account_id: String, start_time: Date)