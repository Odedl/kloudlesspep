package services

import java.text.SimpleDateFormat
import javax.inject.Inject

import models.{KloudlessRecentFilesRequest, KloudlessStub}
import play.api.{Logger, Configuration}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Reads, _}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * Created by oded on 8/24/16.
 */

case class KloudlessRecentFileResponse(total: Int, recent_files_data: Seq[KloudlessRecentFileData])

case class KloudlessRecentFileData(id: String, last_modified: String)


class KloudlessService @Inject()(configuration: Configuration, kloudless_stub: KloudlessStub) {

    implicit val rr: Reads[KloudlessRecentFileData] = (
            (__ \ "id").read[String] and
            (__ \ "modified").read[String]
        ) (KloudlessRecentFileData.apply _)


    implicit val r: Reads[KloudlessRecentFileResponse] = (
            ( __ \ "total").read[Int] and
            ( __ \ "objects").read[Seq[KloudlessRecentFileData]]
        ) (KloudlessRecentFileResponse.apply _)



    val df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")

    def getRecentFiles(get_recent_files_request: KloudlessRecentFilesRequest): Future[KloudlessRecentFileResponse] = {
        validateRequestTimeIsValid(get_recent_files_request)
        val start_time_date_formatted = df.format(get_recent_files_request.start_time)
        kloudless_stub.getRecentFiles(get_recent_files_request, start_time_date_formatted) map( response => {
           response.asOpt[KloudlessRecentFileResponse] match {
               case Some(kloudless_recent_files_response) =>
                   kloudless_recent_files_response
               case None =>
                   throw new Exception
           }

        })

    }

    def validateRequestTimeIsValid(get_recent_files_request: KloudlessRecentFilesRequest) = true
}
